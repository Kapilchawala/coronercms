﻿using Nancy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CoronerCMS.Web.Modules
{
    public class HomeModule : NancyModule
    {
        public HomeModule()
        {
            Get["/"] = _ =>
            {
                dynamic viewBag = new DynamicDictionary();
                viewBag.WelcomeMessage = "Welcome to Coroner CMS";
                return View["home", viewBag];
            };

            Get["/cases"] = _ =>
            {
                dynamic viewBag = new DynamicDictionary();
                viewBag.WelcomeMessage = "Cases";
                return View["cases", viewBag];
            };

            Get["/case"] = _ =>
            {
                dynamic viewBag = new DynamicDictionary();
                viewBag.WelcomeMessage = "Case";
                return View["case", viewBag];
            };


            Get["/reports"] = _ =>
            {
                dynamic viewBag = new DynamicDictionary();
                viewBag.WelcomeMessage = "Reports";
                return View["reports", viewBag];
            };

            Get["/settings"] = _ =>
            {
                dynamic viewBag = new DynamicDictionary();
                viewBag.WelcomeMessage = "Settings";
                return View["settings", viewBag];
            };

            Get["/users"] = _ =>
            {
                dynamic viewBag = new DynamicDictionary();
                viewBag.WelcomeMessage = "Users";
                return View["users", viewBag];
            };

            Get["/user"] = _ =>
            {
                dynamic viewBag = new DynamicDictionary();
                viewBag.WelcomeMessage = "User";
                return View["user", viewBag];
            };


            Get["/newuser"] = _ =>
            {
                dynamic viewBag = new DynamicDictionary();
                viewBag.WelcomeMessage = "New User";
                return View["newuser", viewBag];
            };

            Get["/roles"] = _ =>
            {
                dynamic viewBag = new DynamicDictionary();
                viewBag.WelcomeMessage = "Roles";
                return View["roles", viewBag];
            };

            Get["/signin/"] = _ =>
                {
                    dynamic viewBag = new DynamicDictionary();
                    viewBag.WelcomeMessage = "Signin";
                    return View["signin", viewBag];
                };
        }
    }
}